## Install newest version systemstap
./configure --prefix=/opt/stap --disable-docs \
            --disable-publican --disable-refdocs CFLAGS="-g -O2"
make -j8   # the -j8 option assumes you have about 8 logical CPU cores available
sudo make install

## CMD
```
sudo stap show_one_req.stap -x 5884 -v
```
